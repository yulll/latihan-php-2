<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>

    <?php
        echo "<h3> Soal No 1 Greetings </h3>";

        function greetings($name) {
            echo "Halo $name, Selamat Datang di PKS Digital School!<br>";
        }
        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");

        echo "<h3>Soal No 2 Reverse String</h3>";

        function reverse($name1) {
            $length = strlen($name1);
            $space = "";
            for ($i = ($length - 1); $i >= 0 ; $i--) { 
                $space .= $name1[$i];
            }
            return $space;
        }

        function reverseString($name2) {
            $word = reverse($name2);
            echo $word. "<br>";
        }

        reverseString("abduh");
        reverseString("Digital School");
        reverseString("We Are PKS Digital School Developers");

        echo "<h3>Soal No 3 Palindrome </h3>";

        function palindrome($word1) {
            $reverse = reverse($word1);
            if ($word1 === $reverse)
                echo "true<br>";
            else
                echo "false<br>";
        }

        palindrome("civic");
        palindrome("nababan");
        palindrome("jambaban");
        palindrome("racecar");


        echo "<h3>Soal No 4 Tentukan Nilai </h3>";

        function tentukan_nilai ($angka) {
            $output = "";
            if ($angka >= 85 && $angka < 100) {
                $output .= "Sangat Baik";
            }
            else if ($angka >= 70 && $angka < 85) {
                $output .= "Baik";
            }
            else if ($angka >= 60 && $angka < 70) {
                $output .= "Cukup";
            }
            else 
                $output .= "Kurang";
            return $output . "<br>";
        }
        echo tentukan_nilai(98);
        echo tentukan_nilai(76);
        echo tentukan_nilai(67);
        echo tentukan_nilai(43);
    ?>
</body>

</html>